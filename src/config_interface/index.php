<!DOCTYPE html>
<html>

  <head>
    <meta charset="UTF-8" />
    <title>Interface de configuration − Pyramidion</title>
  </head>

  <body>
    <h1>Configuration de Pyramidion</h1>

      <h2>Connexion Wi-Fi</h2>

        <ul>
          <li><a href="wifi/input_ssid.php">En entrant le nom du réseau</a></li>
          <li><a href="wifi/select_ssid.php">En sélectionnant le nom du réseau parmi les réseaux Wi-Fi proches</a></li>
        </ul>

      <h2>Heures de fonctionnement</h2>
        <ul>
          <li><a href="operating_hours/set_hours.php">Heures de fonctionnement</a></li>
        </ul>

      <h2>Mise à jour</h2>

        <form action="update/upload.php" method="post" enctype="multipart/form-data">
            <input type="file" name="fileToUpload" id="fileToUpload">
            <input type="submit" value="Upload" name="submit">
        </form>

  </body>

</html>
