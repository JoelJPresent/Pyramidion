<?php

$output = shell_exec("mkdir uploads");
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

if(isset($_POST["submit"])) {
    
    if (substr($_FILES["fileToUpload"]["name"], -7) != '.tar.gz') {
        echo "<p>Désolé, votre fichier doit être un .tar.gz.</p>";
        $uploadOk = 0;
    }
    
    if (file_exists($target_file)) {
        echo "<p>Désolé, le fichier existe déjà.</p>";
        $uploadOk = 0;
    }
    if ($uploadOk == 1)
    {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            $output = shell_exec("tar -xzvf uploads/" . $_FILES["fileToUpload"]["name"]);
            echo "<p>Le fichier ". htmlspecialchars(basename( $_FILES["fileToUpload"]["name"])) . " a été chargé.</p>";
        } else {
            echo "<p>Désolé, il y a eu une erreur durant le chargement de votre fichier.</p>";
        }
    }
}
?>
