<!DOCTYPE html>
<html>

<?php setlocale(LC_CTYPE, "fr_FR.UTF-8"); ?>

<head>
    <meta charset="UTF-8" />
    <title>Login Wi-Fi − Pyramidion</title>
</head>

<body>

    <h1>Login Wi-Fi</h1>

        <form method="post" action="wifi_connection.php">
            <label for="ssid">Nom du réseau</label> : 
            <select name="ssid" id="ssid">
                <?php
                    $cmd = "sudo iwlist wlan0 scan | grep 'ESSID:' | sort | uniq";
                    exec ($cmd, $output);
                    print_r ($output);
                    foreach ($output as $line) {
                        preg_match('/ESSID:"(.*)"/', $line, $matches);
                        if (!empty($matches)) {
                            $ssid_name = htmlspecialchars($matches[1]);
                            echo '<option value="'.$ssid_name.'">'.$ssid_name.'</option>';
                        }
                        echo '<br>';
                    }
                ?>
            </select>

            <label for="psk">Clé de sécurité</label> : 
            <input type="password" name="psk" id="psk" /><br>

            <input type="submit" value="Se connecter">
        </form>

</body>

</html>