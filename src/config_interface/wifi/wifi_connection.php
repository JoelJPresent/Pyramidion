<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <title>Connexion au Wi-Fi − Pyramidion</title>
</head>

<body>
    <p><?php
        setlocale(LC_CTYPE, "fr_FR.UTF-8");

        $ssid = escapeshellarg($_POST['ssid']);
        $psk = escapeshellarg($_POST['psk']);

        $cmd = "wpa_passphrase ".$ssid." ".$psk." | sudo tee -a /etc/wpa_supplicant/wpa_supplicant.conf >> /dev/null";
        system ($cmd);

        $ssid_html = htmlspecialchars($_POST['ssid']);
        echo '<p>Connexion au réseau “'.$ssid_html.'”.</p>';
    ?></p>
</body>

</html>