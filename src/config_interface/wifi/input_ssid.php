<!DOCTYPE html>
<html>

<?php setlocale(LC_CTYPE, "fr_FR.UTF-8"); ?>

<head>
    <meta charset="UTF-8" />
    <title>Login Wi-Fi − Pyramidion</title>
</head>

<body>

    <h1>Login Wi-Fi</h1>

        <form method="post" action="wifi_connection.php">
            <label for="ssid">Nom du réseau</label> : 
            <input type="text" name="ssid" id="ssid" required /><br>

            <label for="psk">Clé de sécurité</label> : 
            <input type="password" name="psk" id="psk" /><br>

            <input type="submit" value="Se connecter">
        </form>

</body>

</html>