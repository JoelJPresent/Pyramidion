<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <title>Heures de fonctionnement</title>
</head>

<body>
    <p><?php
        function push_to_cron($str) {
            system("echo ".escapeshellarg($str)." | sudo tee -a ~pi/cron.tab >> /dev/null");
        }

        preg_match('/([0-9]{2}):([0-9]{2})/', $_POST['start'], $start);
        $start_hour = intval($start[1], 10);
        $start_min = intval($start[2], 10);
        $start_html = htmlspecialchars($_POST['start']);

        preg_match('/([0-9]{2}):([0-9]{2})/', $_POST['stop'], $stop);
        $stop_hour = intval($stop[1], 10);
        $stop_min = intval($stop[2], 10);
        $stop_html = htmlspecialchars($_POST['stop']);

        if ($start_hour < 0 || $start_hour > 23 || $start_min < 0 || $start_min > 59)
        {
            echo "<p>Erreur ! L'heure de démarrage (".$start_html.") est incorrecte.</p>";
        }
        elseif ($stop_hour < 0 || $stop_hour > 23 || $stop_min < 0 || $stop_min > 59)
        {
            echo "<p>Erreur ! L'heure d'arrêt (".$stop_html.") est incorrecte.</p>";
        }
        else
        {
            system("echo '# Format = min heure jour-du-mois mois jour-semaine' | sudo tee ~pi/cron.tab >> /dev/null");
            push_to_cron("# ");
            push_to_cron("# exemples:");
            push_to_cron("# ");
            push_to_cron("# 0 8 * * * -> tous les jours a 8h");
            push_to_cron("# 0 * * * * -> toutes les heures");
            push_to_cron("# ");
            $start_cmd = $start_min." ".$start_hour." * * * sudo ~pi/pyramidion-gpio-start.sh";
            $stop_cmd = $stop_min." ".$stop_hour." * * * sudo ~pi/pyramidion-gpio-stop.sh";
            push_to_cron($start_cmd);
            push_to_cron($stop_cmd);

            echo "<p>Les heures de fonctionnement ont été configurées :</p>";
            echo "<ul>";
            echo "<li>Démarrage à ".$start_html."</li>";
            echo "<li>Arrêt à ".$stop_html."</li>";
            echo "</ul>";
        }

    ?></p>
</body>

</html>