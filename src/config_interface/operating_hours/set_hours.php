<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <title>Heures de fonctionnement − Pyramidion</title>
</head>

<body>
  <h1>Heures de fonctionnement</h1>
      <form method="post" action="hours_setting.php">

        <label for="start">Heure de démarrage</label> :
        <input type="time" name="start" id="start" required /><br>

        <label for="stop">Heure d'arrêt</label> :
        <input type="time" name="stop" id="stop" required /><br>

        <input type="submit" value="Valider">
      </form>
</body>
</html>