#!/bin/bash

cd ../GPIO/gpioIrq/
make
sudo make install

cp ../../../scripts/master/pyramidion* /home/pi

sudo cp ../../../scripts/master/*.service /etc/systemd/system
sudo systemctl enable pyramidion-button.service
crontab /home/pi/cron.tab

cd ../rpi_gpio
make
sudo make install

cp ../../../scripts/slave/pyramidion-receive.service /home/pi
cp ../../../scripts/slave/pyramidion-receive.sh /home/pi

sudo cp ../../../scripts/slave/pyramidion-receive.service /etc/systemd/system
sudo systemctl enable ../../../scripts/slave/pyramidion-receive.service
